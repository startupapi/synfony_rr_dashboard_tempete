
$( document ).ready(function() {

	calculateStepSpace()

	$( window ).resize(function() {
		calculateStepSpace()
	});

	function calculateStepSpace() {
		var wizardCountStep = $( ".wizard-steps" ).children().length;
		var withWizard = $( ".wizard-steps" ).width()
		var wizardSizeStep1 = 100;
		if($( window ).width() <= 768){
			wizardSizeStep1 = 50
		}else if($( window ).width() >= 1200){
			wizardSizeStep1 = 150
		}
		var wizardStepSpace = (withWizard / wizardCountStep) + (wizardSizeStep1 / 2)
		$( ".wizard-steps .wizard-step-number.step2" ).attr("style","left: " + wizardStepSpace + "px !important;")
		$( ".wizard-steps .wizard-step-number.step3" ).attr("style","left: " + (wizardStepSpace * 2) + "px !important;")
	}

});